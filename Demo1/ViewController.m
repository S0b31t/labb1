//
//  ViewController.m
//  Demo1
//
//  Created by Johan H on 2015-01-20.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchImage {
    self.view.backgroundColor = [UIColor blackColor];
  
}

- (IBAction)switchBackground {
    self.view.backgroundColor = [UIColor whiteColor];
}

- (IBAction)switchen {
    
    if (!(self.view.backgroundColor ==[UIColor whiteColor]))
    {
    self.view.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.view.backgroundColor = [UIColor brownColor];
    }
}
@end
