//
//  ViewController.h
//  Demo1
//
//  Created by Johan H on 2015-01-20.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)switchImage;
- (IBAction)switchBackground;
- (IBAction)switchen;

@end

